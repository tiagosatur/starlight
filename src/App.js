import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Normalize from "react-normalize";
import { Provider } from "react-redux";
import { initDB } from "react-indexed-db";

import { GlobalStyle } from "./style";
import store from "./store";
import { indexedDBConfig } from "./utils";

import { PrivateRoute, PublicRoute } from "./hocs";

import { Login, Home, NotFound, Person } from "./screens";
initDB(indexedDBConfig);

function App() {
  return (
    <Provider store={store}>
      <Normalize />
      <GlobalStyle />
      <Router>
        <Switch>
          <PrivateRoute component={Home} path="/" exact />
          <Route component={Login} path="/auth" />
          <PublicRoute component={Person} path="/person/:personId" />
          <PublicRoute component={NotFound} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
