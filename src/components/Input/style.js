import { css } from "styled-components";

export const style = css`
  align-items: center;
  border: 0.1rem solid transparent;
  box-shadow: none;
  display: inline-flex;
  font-size: 1.6rem;
  height: 3.2rem;
  justify-content: flex-start;
  padding: 1.6rem;
  transition: all 0.4s;
  width: 100%;
`;
