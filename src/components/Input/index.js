import React, { useState } from "react";
import styled from "styled-components";
import { colors, boxShadow, blackNeomorphismShadow } from "../../style";
import { style } from "./style";

const Input = props => {
  const [value, setValue] = useState("");
  const {
    classes,
    placeholder,
    id,
    name,
    disabled,
    type,
    register,
    errors,
    handleKeyUp,
    handleChange,
    handleKeyDown
  } = props;

  return (
    <Field>
      <StyledInput
        type={type ? type : "text"}
        id={id && id}
        name={name}
        ref={register}
        errors={errors}
        className={`input ${classes ? classes : ""}`}
        placeholder={placeholder}
        disabled={disabled}
        onKeyUp={handleKeyUp}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
      />
      {errors[name] ? (
        <StyledSpan errors={errors} name={name} className="field-error">
          {errors[name] && errors[name].message}
        </StyledSpan>
      ) : null}
    </Field>
  );
};

const Field = styled.div`
  margin-bottom: 1.6rem;
`;

const StyledInput = styled.input`
  ${style}
  ${blackNeomorphismShadow}
  color: #f5f5f5;

  &:active,
  &:focus {
    box-shadow: ${boxShadow.active};
  }

  &::placeholder {
    color: #f5f5f5;
  }
`;

const StyledSpan = styled.span`
  color: red;
  display: ${({ errors, name }) => {
    return errors[name] && errors[name].message ? "flex" : "none";
  }};
  font-size: 1.4rem;
  padding: 0.4rem 0.8rem 0 0.8rem;
  transition: all 0.5s;
`;

export default Input;
