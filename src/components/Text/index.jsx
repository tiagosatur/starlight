import styled from 'styled-components';
import { colors } from '../../style';

export default styled.p`
  color: ${
    ({ primary, secondary, white }) => 
      secondary ? 
        colors.secondary : 
      white ?
        colors.white :
      colors.primary
    };
  display: block;
  font-family: Lekton;
  font-size: 1.6rem;
  font-weight: 300;
  margin: 0 0 0.4rem 0;
  margin-bottom: ${({mb}) => mb ? `${mb}rem` : 0 };
  text-align: ${({ center }) => center ? 'center' : 'justify'};
  word-wrap: break-word;
`;

