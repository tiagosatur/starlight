import React from "react";
import styled, { css } from "styled-components";
import { ThreeBounce } from "styled-spinkit";

const ThreeBounceLoader = ({ ...props }) => <StyledLoader {...props} />;

const StyledLoader = styled(ThreeBounce)`
  ${({ isAutocomplete }) =>
    isAutocomplete &&
    css`
      position: absolute;
      right: 0;
      top: -70%;
    `};
`;

export default ThreeBounceLoader;
