import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { IoMdPlanet } from "react-icons/io";

import { colors } from "../../style";
import { LogoImage } from "../../assets";
import { Autocomplete, Navbar } from "..";

const Header = ({ data: { routerProps } }) => {
  return (
    <HeaderContainer>
      <Autocomplete data={{ routerProps }} />
      <Navbar />
      <LogoContainer>
        <Logo size="5rem" color={colors.primary} />
      </LogoContainer>
    </HeaderContainer>
  );
};

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: column-reverse;
  align-items: center;
  padding: 3.2rem 0 8.2rem 0;
`;
const LogoContainer = styled.div``;

const Logo = styled(IoMdPlanet)`
  transform: rotate(-67deg);
`;

export default Header;
