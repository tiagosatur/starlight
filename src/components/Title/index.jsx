import styled from 'styled-components';


export default styled.h2`
  color: ${({ color }) => color};
  font-family: Raleway;
  font-weight: 300;
`;

