import styled, { css } from "styled-components";
import { colors, border } from "../../style";

const buttonBase = css`
  align-items: center;
  background-color: transparent;
  border-color: transparent;
  border-radius: 0.3rem;
  border-style: solid;
  border-width: 0.1rem;
  color: ${colors.black};
  cursor: pointer;
  font-size: 1.4rem;
  font-weight: bold;
  display: inline-flex;
  justify-content: center;
  padding: 0.8rem 1.6rem;
  position: relative;
  transition: all 0.5s;
`;

export const StyledButton = styled.button`
  ${buttonBase}
  
  color: ${({ solid, outlined, text, anchor }) =>
    solid
      ? colors.dark
      : text
      ? colors.black
      : outlined || anchor
      ? colors.primary
      : colors.black};

  background: ${({ solid, text, anchor, outlined }) =>
    solid
      ? colors.green
      : outlined || anchor || text
      ? colors.transparent
      : colors.transparent};
  
  border-color: ${({ outlined, primary, secondary }) =>
    outlined && primary
      ? colors.primary
      : outlined && secondary
      ? colors.secondary
      : colors.transparent};
  border-radius: ${border.radius.default};

  padding: ${({ rounded }) => (rounded ? "0.8rem" : "0.8rem" + " " + "1.6rem")};

  &:hover {
    background-color: rgba(white, .2);
  }

  a {
    color: ${({ text, anchor }) =>
      text ? colors.black : anchor ? colors.primary : colors.black};
  }
`;
