import React from "react";
import { render } from "@testing-library/react";
import Button from ".";

const onButtonClick = jest.fn(() => {});

describe("Button tests", () => {
  const { getByTestId } = render(<Button solid handleClick={onButtonClick} />);

  it("renders the Button component", () => {
    const btn = getByTestId("btn-testing");
    expect(btn).toBeInTheDocument();
  });
});
