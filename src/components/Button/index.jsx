import React from "react";
import { StyledButton } from "./StyledButton";

const Button = ({
  type,
  handleClick,
  classes,
  iconName,
  children,
  disabled,
  title,
  ...styledProps
}) => {
  const defineType = !type || type === "" ? "submit" : type;

  return (
    <StyledButton
      type={defineType}
      onClick={handleClick}
      className={`btn ${classes ? classes : ""}`}
      disabled={disabled}
      title={title ? title : ""}
      data-testid="btn-testing"
      {...styledProps}
    >
      {iconName ? <i className={iconName} /> : null}
      {children}
    </StyledButton>
  );
};

export default Button;
