import React from "react";
import { RouterLink } from "..";
import styled from "styled-components";

import { device } from "../../style";

const Navbar = () => {
  return (
    <nav>
      <NavList>
        <li>
          <RouterLink activeOnlyWhenExact={true} to="/" label="Home" />
        </li>
        <li>
          <RouterLink to="/planets" label="Planets" />
        </li>
        <li>
          <RouterLink to="/species" label="Species" />
        </li>
      </NavList>
    </nav>
  );
};

const NavList = styled.ul`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 3.2rem;

  @media ${device.tablet} {
    margin-bottom: 4.2rem;
  }

  a {
    display: inline-flex;
    padding: 8px 32px;
    margin: 0 1.6rem;
  }
`;

export default Navbar;
