import React, { useState, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import { Link } from "react-router-dom";
import { debounce } from "lodash";

import { useAction, useStore, useDebounce } from "../../utils/hooks";
import { colors, blackNeomorphismShadow, device } from "../../style";
import { ThreeBounceLoader } from "../../components";

const Autocomplete = ({
  data: {
    routerProps: { history }
  }
}) => {
  const { actions } = useAction();

  const {
    searchReducer: { suggestions, count, isLoading }
  } = useStore();

  const [activeSuggestion, setActiveSuggestion] = useState(0);
  const [filteredSuggestions, setFilteredSuggestions] = useState([]);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [
    suggestionsNotFoundVisibility,
    setSuggestionsNotFoundVisibility
  ] = useState(false);

  const debouncedSearchTerm = useDebounce(searchTerm, 500);
  const debouncedTerm = useCallback(debounce(actions.search, 1000), []);

  useEffect(() => {
    searchTerm !== "" && debouncedTerm(searchTerm);
    searchTerm === "" && resetAutocompleteState();
  }, [debouncedTerm, searchTerm]);

  useEffect(() => {
    setSuggestionsNotFoundVisibility(false);
  }, []);

  // useEffect(() => {
  //   if (debouncedSearchTerm !== "") {
  //     actions.search(searchTerm);
  //   } else {
  //     resetAutocompleteState();
  //   }
  // }, [debouncedSearchTerm]);

  useEffect(() => {
    const matchResults = suggestions.filter(
      suggestion =>
        suggestion.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
    );

    if (suggestions.length > 0 && matchResults.length > 0) {
      setActiveSuggestion(0);
      setFilteredSuggestions(matchResults);
      setShowSuggestions(true);
    }

    count === 0 && setSuggestionsNotFoundVisibility(true);
  }, [suggestions]);

  const handleChange = e => {
    const { value } = e.currentTarget;
    setSearchTerm(value);
  };

  const getPersonIdFromUrl = obj => {
    const getPerson = suggestions.filter(
      suggestion =>
        suggestion.name == filteredSuggestions[activeSuggestion].name
    );
    const personId = getPerson[0].url.match(/\d+/)[0];
    return personId;
  };

  const resetAutocompleteState = () => {
    actions.resetSuggestions();
    setActiveSuggestion(0);
    setShowSuggestions(false);
    setSearchTerm("");
    setFilteredSuggestions([]);
    setSuggestionsNotFoundVisibility(false);
  };

  const handleKeyDown = e => {
    switch (e.keyCode) {
      // Backspace
      case 8:
        if (searchTerm.length === 1) resetAutocompleteState();
        break;

      // Enter
      case 13:
        setSearchTerm(filteredSuggestions[activeSuggestion].name);
        const getPerson = suggestions.filter(
          suggestion =>
            suggestion.name == filteredSuggestions[activeSuggestion].name
        );
        const personId = getPerson[0].url.match(/\d+/)[0];

        resetAutocompleteState();
        return history.push(`/person/${personId}`);

      // Arrow up
      case 38:
        if (activeSuggestion === 0) return;
        setActiveSuggestion(activeSuggestion - 1);
        break;

      // Arrow down
      case 40:
        if (activeSuggestion - 1 === filteredSuggestions.length) return;
        setActiveSuggestion(activeSuggestion + 1);
        break;

      // Esc
      case 27:
        resetAutocompleteState();
        break;

      case 8:
      case 46:
        if (searchTerm === "") resetAutocompleteState();
        break;
    }
  };

  return (
    <AutocompleteContainer>
      <InputContainer>
        <Input
          type="text"
          value={searchTerm}
          onChange={handleChange}
          placeholder="Search a human..."
          onKeyDown={handleKeyDown}
          onBlur={() => setSuggestionsNotFoundVisibility(false)}
          data-testid="starlight-main-search-input"
        />
        {isLoading && <ThreeBounceLoader isAutocomplete color={colors.green} />}
      </InputContainer>

      {suggestions.length > 0 && showSuggestions && (
        <SuggestionList>
          {filteredSuggestions.length &&
            filteredSuggestions.map((suggestion, index) => {
              let isSuggestionActive = false;

              if (index === activeSuggestion) {
                isSuggestionActive = true;
              }
              const personId = suggestion.url.match(/\d+/)[0];

              return (
                <Link
                  to={`/person/${personId}`}
                  onClick={resetAutocompleteState}
                  key={suggestion.url}
                >
                  <SuggestionItem isSuggestionActive={isSuggestionActive}>
                    {suggestion.name}
                  </SuggestionItem>
                </Link>
              );
            })}
        </SuggestionList>
      )}
      {suggestionsNotFoundVisibility && (
        <NoSuggestions>
          We didn't find any suggestions, try other term!
        </NoSuggestions>
      )}
    </AutocompleteContainer>
  );
};

const AutocompleteContainer = styled.div`
  display: flex;
  justify-content: center;
  position relative;
  width: 100%;
`;

const sameBreakpointWidth = css`
  width: 90%;

  @media ${device.tablet} {
    width: 80%;
  }

  @media ${device.laptop} {
    width: 50rem;
  }
`;

const SuggestionList = styled.ul`
  ${blackNeomorphismShadow};
  ${sameBreakpointWidth};
  border-radius: 0.2rem;
  border-top-width: 0;
  list-style: none;
  margin-top: 0;
  max-height: 20rem;
  overflow-y: auto;
  padding-left: 0;
  position: absolute;
  top: 5rem;
  z-index: 50;

  li {
    padding: 0.5rem;
  }
`;

const SuggestionItem = styled.li`
  background-color: ${({ isSuggestionActive }) =>
    isSuggestionActive ? colors.dark : colors.dark02};
  color: ${({ isSuggestionActive }) =>
    isSuggestionActive ? colors.green : colors.primary};
  cursor: pointer;
  font-weight: ${({ isSuggestionActive }) => (isSuggestionActive ? 700 : 400)};
`;

const NoSuggestions = styled.div`
  ${blackNeomorphismShadow};
  background-color: ${colors.dark02};
  color: ${colors.primary};
  padding: 1.6rem;
  position: absolute;
  top: 5rem;
  z-index: 30;
`;

const Input = styled.input`
  ${blackNeomorphismShadow};
  background: ${colors.dark02};
  border: 0;
  color: ${colors.primary};
  outline: none;
  padding: 1.2rem;
  width: 100%;

  &::placeholder {
    color: ${colors.primary};
  }
`;

const InputContainer = styled.div`
  ${sameBreakpointWidth};
  position: relative;
`;

export default Autocomplete;
