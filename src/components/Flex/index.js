import styled from 'styled-components';

const Flex = styled.div`
    align-items: ${({ alignItems }) => alignItems};
    display: flex;
    direction: ${({ column }) => column ? 'column' : 'row'};
    flex-wrap: ${({ wrap }) => wrap ? 'wrap' : 'no-wrap'};
    justify-content: ${({ justify }) => justify};
`;

export default Flex;