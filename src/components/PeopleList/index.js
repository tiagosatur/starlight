import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { WiAlien } from "react-icons/wi";

import { getPersonId, uniqueId } from "../../utils";
import { colors, blackNeomorphismShadow, device } from "../../style";
import { Text, ThreeBounceLoader } from "..";

const PeopleList = ({
  data: {
    list,
    areAvatarsLoading,
    routerProps,
    routerProps: { history, location, match }
  },
  actions: { handleSelectFile, saveFile }
}) => {
  return (
    <List>
      {list &&
        list.map(person => {
          const personId = getPersonId(person.url);
          const avatar = person.avatar;
          const uid = uniqueId();

          return (
            <Item key={uid}>
              <Anchor to={`/person/${personId}`} key={person.url}>
                <ItemHeader>
                  {areAvatarsLoading ? (
                    <ThreeBounceLoader color={colors.green} />
                  ) : avatar ? (
                    <AvatarContainer img={avatar}>
                      {" "}
                      {/* <Avatar src={avatar} /> */}
                    </AvatarContainer>
                  ) : (
                    <WiAlien
                      size="5rem"
                      color={colors.primary}
                      className="icon"
                    />
                  )}
                </ItemHeader>

                <ItemBody>
                  <Name white color={colors.green}>
                    {person.name}
                  </Name>
                </ItemBody>
              </Anchor>
            </Item>
          );
        })}
    </List>
  );
};

const View = styled.div`
  cursor: pointer;
  &:hover {
    p {
      color: deeppink;
    }
  }
`;
const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
  transition: all 0.6s;
`;

const Item = styled.li`
  ${blackNeomorphismShadow};
  border: 0.1rem solid rgba(0, 0, 0, 0.3);
  border-radius: 1.6rem;
  margin: 3.2rem 0.8rem;
  width: 45%;

  overflow: hidden;

  @media ${device.tablet} {
    width: 30.33%;
  }

  @media ${device.laptop} {
    width: 23.2%;
  }
`;

const Anchor = styled(Link)`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  min-height: 15rem;

  &:hover {
    .icon {
      fill: #333;
    }
  }

  .icon {
    transition: all 0.6s;
  }
`;

const ItemHeader = styled.header`
  align-items: center;
  background-color: rgba(0, 0, 0, 0.3);
  display: flex;
  flex-grow: 1;
  justify-content: center;
  min-height: 15rem;
  margin-bottom: 0.8rem;
  width: 100%;

  @media ${device.tablet} {
    min-height: 22rem;
  }
`;

const ItemBody = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;
  padding: 1.6rem;
  width: 100%;
`;

const Name = styled(Text)`
  color: ${({ color }) => color};
`;

const Avatar = styled.img`
  height: auto;
  width: 100%;
`;

const AvatarContainer = styled.div`
  background: url(${({ img }) => img && img});
  background-size: cover;
  box-shadow: inset 0.1rem 0.2rem 2rem rgba(0, 0, 0, 0.4),
    1.4rem 0rem 0.8rem rgba(0, 0, 0, 0.8);
  background-repeat: no-repeat;
  height: 15rem;
  width: 100%;

  @media ${device.tablet} {
    min-height: 22rem;
  }
`;

export default PeopleList;
