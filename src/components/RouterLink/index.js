import React from "react";
import { Link, useRouteMatch } from "react-router-dom";
import styled from "styled-components";
import { colors, blackNeomorphismShadow } from "../../style";

export default function RouterLink({ label, to, activeOnlyWhenExact }) {
  let match = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact
  });

  return (
    <StyledLink to={to} active={match} className={match ? "active" : ""}>
      {label}
    </StyledLink>
  );
}

const StyledLink = styled(Link)`
  ${({ active }) => active && blackNeomorphismShadow};
  color: ${({ active }) => (active ? colors.green : colors.primary)};
  transition: all 0.4s;
`;
