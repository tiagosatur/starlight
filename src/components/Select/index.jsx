import React from "react";
import styled from "styled-components";
import { colors, blackNeomorphismShadow } from "../../style";

const Select = ({
  name,
  handleChange,
  value,
  placeholder,
  list,
  optionValue,
  optionLabel,
  uniqueKey
}) => {
  return (
    <div>
      <StyledSelect name={name} onChange={handleChange} value={value}>
        {placeholder && (
          <option value="" disabled>
            {placeholder}
          </option>
        )}
        <option key="23456789" value="all">
          All
        </option>
        {list &&
          list.map(person => (
            <option key={person[uniqueKey]} value={person[optionValue]}>
              {person[optionLabel][0].toUpperCase() +
                person[optionLabel].slice(1)}
            </option>
          ))}
      </StyledSelect>
    </div>
  );
};

const StyledSelect = styled.select`
  ${blackNeomorphismShadow};
  background: ${colors.dark02};
  border: 0;
  color: ${colors.primary};
  cursor: pointer;
  padding: 0.8rem 2rem;
`;

export default Select;
