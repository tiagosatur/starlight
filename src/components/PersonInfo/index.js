import React from "react";
import styled from "styled-components";

import { Title, Text, ThreeBounceLoader } from "..";
import { colors, blackNeomorphismShadow } from "../../style";

const PersonInfo = ({ data: { person, isPersonLoading } }) => {
  return (
    <Container>
      {isPersonLoading ? (
        <ThreeBounceLoader color={colors.green} />
      ) : (
        <>
          <Title color={colors.green}>{person.name}</Title>
          <Text>
            <Label>Eye color</Label>
            {person.eye_color}
          </Text>
          <Text>
            <Label>Gender</Label>
            {person.gender}
          </Text>
          {person.hair_color !== "n/a" && (
            <Text>
              {" "}
              <Label>Gender</Label>
              {person.hair_color}
            </Text>
          )}
          <Text>
            <Label>Birth year</Label>
            {person.birth_year}
          </Text>
        </>
      )}
    </Container>
  );
};

const Container = styled.div`
  ${blackNeomorphismShadow};
  padding: 3.2rem;
  margin-top: 3rem;
  min-height: 25rem;
`;

const Label = styled.span`
  display: inline-flex;
  font-weight: 700;
  margin-right: 0.4rem;
  width: 30%;
`;

export default PersonInfo;
