import React from "react";
import { render } from "@testing-library/react";

import App from "./App";

it("renders App withouth crashing", () => {
  const { getByTestId } = render(<App />);
  const searchInput = getByTestId("starlight-main-search-input");
  expect(searchInput).toBeInTheDocument();
});
