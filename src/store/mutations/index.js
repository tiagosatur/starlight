import * as TYPES from "../actionTypes";

export const mutations = {
  [TYPES.SIGNIN_PENDING]: { type: TYPES.SIGNIN_PENDING },
  [TYPES.SIGNIN_SUCCESS](data) {
    return { type: TYPES.SIGNIN_SUCCESS, payload: data };
  },
  [TYPES.SIGNIN_FAIL](error) {
    return { type: TYPES.SIGNIN_FAIL, payload: error };
  },

  [TYPES.SIGNUP_PENDING]: { type: TYPES.SIGNUP_PENDING },
  [TYPES.SIGNUP_SUCCESS](data) {
    return { type: TYPES.SIGNUP_SUCCESS, payload: data };
  },
  [TYPES.SIGNUP_FAIL](error) {
    return { type: TYPES.SIGNUP_FAIL, payload: error };
  },

  [TYPES.LOGOUT_PENDING]: { type: TYPES.LOGOUT_PENDING },
  [TYPES.LOGOUT_SUCCESS]: { type: TYPES.LOGOUT_SUCCESS },
  [TYPES.LOGOUT_FAIL](error) {
    return { type: TYPES.LOGOUT_FAIL, payload: error };
  },

  [TYPES.GET_PEOPLE_PENDING]: { type: TYPES.GET_PEOPLE_PENDING },
  [TYPES.GET_PEOPLE_SUCCESS](peopleList) {
    return { type: TYPES.GET_PEOPLE_SUCCESS, payload: peopleList };
  },
  [TYPES.GET_PEOPLE_FAIL](error) {
    return { type: TYPES.GET_PEOPLE_FAIL, payload: error };
  },

  [TYPES.SET_PEOPLE_AVATAR_PENDING]: { type: TYPES.SET_PEOPLE_AVATAR_PENDING },

  [TYPES.SET_PEOPLE_AVATAR_SUCCESS](data) {
    return { type: TYPES.SET_PEOPLE_AVATAR_SUCCESS, payload: data };
  },
  [TYPES.SET_PEOPLE_AVATAR_FAIL](data) {
    return { type: TYPES.SET_PEOPLE_AVATAR_FAIL, payload: data };
  },

  [TYPES.GET_PERSON_PENDING]: { type: TYPES.GET_PERSON_PENDING },
  [TYPES.GET_PERSON_SUCCESS](data) {
    return { type: TYPES.GET_PERSON_SUCCESS, payload: data };
  },
  [TYPES.GET_PERSON_FAIL](error) {
    return { type: TYPES.GET_PERSON_FAIL, payload: error };
  },
  [TYPES.SET_PERSON_AVATAR](base64Image) {
    return { type: TYPES.SET_PERSON_AVATAR, payload: base64Image };
  },

  [TYPES.GET_PLANETS_PENDING]: { type: TYPES.GET_PLANETS_PENDING },
  [TYPES.GET_PLANETS_SUCCESS](data) {
    return { type: TYPES.GET_PLANETS_SUCCESS, payload: data };
  },
  [TYPES.GET_PLANETS_FAIL](error) {
    return { type: TYPES.GET_PLANETS_FAIL, payload: error };
  },

  [TYPES.GET_SPECIES_PENDING]: { type: TYPES.GET_SPECIES_PENDING },
  [TYPES.GET_SPECIES_SUCCESS](data) {
    return { type: TYPES.GET_SPECIES_SUCCESS, payload: data };
  },
  [TYPES.GET_SPECIES_FAIL](error) {
    return { type: TYPES.GET_SPECIES_FAIL, payload: error };
  },

  [TYPES.POST_SEARCH_PENDING]: { type: TYPES.POST_SEARCH_PENDING },
  [TYPES.POST_SEARCH_SUCCESS](data) {
    return { type: TYPES.POST_SEARCH_SUCCESS, payload: data };
  },
  [TYPES.POST_SEARCH_FAIL](error) {
    return { type: TYPES.POST_SEARCH_FAIL, payload: error };
  },
  [TYPES.RESET_SEARCH_SUGGESTIONS]: { type: TYPES.RESET_SEARCH_SUGGESTIONS }
};
