import api from "../../api";
import * as TYPES from "../actionTypes";
import { mutations } from "../mutations";

export const search = term => dispatch => {
  dispatch(mutations[TYPES.POST_SEARCH_PENDING]);
  if (term !== "") {
    return api.get
      .searchPeopleService(term)
      .then(res => {
        if (res.status < 200 && res.status > 299) return Promise.reject(res);
        dispatch(
          mutations[TYPES.POST_SEARCH_SUCCESS]({
            count: res.data.count,
            results: res.data.results
          })
        );
      })
      .catch(error => {
        dispatch(mutations[TYPES.POST_SEARCH_FAIL](error.data.message));
      });
  }
};

export const resetSuggestions = () => dispatch => {
  dispatch(mutations[TYPES.RESET_SEARCH_SUGGESTIONS]);
};
