import api from '../../api';
import * as TYPES from '../actionTypes';
import { mutations } from '../mutations';


export const signUp = values => dispatch => {
    dispatch(mutations[TYPES.SIGNUP_PENDING]);
    return api.post.signUpService(values)
    .then(res => { if(res.success) dispatch(mutations[TYPES.SIGNUP_SUCCESS](res)) })
    .catch(error => { dispatch(mutations[TYPES.SIGNUP_FAIL](error)) });
}

export const signIn = values => dispatch => {
    dispatch(mutations[TYPES.SIGNIN_PENDING]);
    return api.post.signInService(values)
    .then(res => { if(res.success) dispatch(mutations[TYPES.SIGNIN_SUCCESS](res)) })
    .catch(error => { dispatch(mutations[TYPES.SIGNIN_FAIL](error)) });
}

export const logout = () => dispatch => {
    dispatch(mutations[TYPES.LOGOUT_PENDING]);
    return api.post.logoutService()
      .then(res => { if(res.success) dispatch(mutations[TYPES.LOGOUT_SUCCESS](res)) })
      .catch(error => { dispatch(mutations[TYPES.LOGOUT_FAIL](error)) });
}