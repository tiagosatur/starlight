import api from "../../api";
import * as TYPES from "../actionTypes";
import { mutations } from "../mutations";

export const getPerson = personId => dispatch => {
  dispatch(mutations[TYPES.GET_PERSON_PENDING]);
  return api.get
    .personService(personId)
    .then(res => {
      if (res.status < 200 && res.status > 299) Promise.reject(res);
      dispatch(mutations[TYPES.GET_PERSON_SUCCESS](res.data));
      return Promise.resolve(res.data);
    })
    .catch(error => {
      debugger;
      dispatch(mutations[TYPES.GET_PERSON_FAIL](error.data));
    });
};

export const setPersonAvatar = base64Image => dispatch => {
  dispatch(mutations[TYPES.SET_PERSON_AVATAR](base64Image));
};
