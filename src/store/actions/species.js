import api from '../../api';
import * as TYPES from '../actionTypes';
import { mutations } from '../mutations';


export const getSpecies = () => dispatch => {
    dispatch(mutations[TYPES.GET_SPECIES_PENDING]);
    return api.get.speciesService()
    .then(res => { 
        if(res.status < 200 && res.status > 299) Promise.reject(res);
        dispatch(mutations[TYPES.GET_SPECIES_SUCCESS](res.data.results));
    })
    .catch(error => { dispatch(mutations[TYPES.GET_SPECIES_FAIL](error.data.message)) });
}