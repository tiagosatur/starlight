import api from "../../api";
import * as TYPES from "../actionTypes";
import { mutations } from "../mutations";

export const getPeople = () => dispatch => {
  dispatch(mutations[TYPES.GET_PEOPLE_PENDING]);
  return api.get
    .peopleService()
    .then(res => {
      res.status < 200 && res.status > 299 && Promise.reject(res);
      dispatch(mutations[TYPES.GET_PEOPLE_SUCCESS](res.data.results));
      return Promise.resolve(res);
    })
    .catch(error => {
      dispatch(mutations[TYPES.GET_PEOPLE_FAIL](error));
    });
};

export const setPeopleAvatar = data => dispatch => {
  dispatch(mutations[TYPES.SET_PEOPLE_AVATAR_PENDING]);
  setTimeout(() => {
    data.error
      ? dispatch(mutations[TYPES.SET_PEOPLE_AVATAR_FAIL](data))
      : dispatch(mutations[TYPES.SET_PEOPLE_AVATAR_SUCCESS](data));
  }, 2000);
};
