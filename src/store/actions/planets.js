import api from '../../api';
import * as TYPES from '../actionTypes';
import { mutations } from '../mutations';


export const getPlanets = () => dispatch => {
    dispatch(mutations[TYPES.GET_PLANETS_PENDING]);
    return api.get.peopleService()
    .then(res => { 
        if(res.status < 200 && res.status > 299) Promise.reject(res);
        dispatch(mutations[TYPES.GET_PLANETS_SUCCESS](res.data.results));
    })
    .catch(error => { dispatch(mutations[TYPES.GET_PLANETS_FAIL](error.data.message)) });
}