import { combineReducers, createStore, applyMiddleware  } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import {
    peopleReducer,
    planetsReducer,
    speciesReducer,
    authReducer,
    personReducer,
    searchReducer,
} from './reducers';

const middlewares = [thunk]

export default createStore(
    combineReducers({
        authReducer,
        peopleReducer,
        planetsReducer,
        speciesReducer,
        personReducer,
        searchReducer
}), 
    composeWithDevTools(
        applyMiddleware(...middlewares)
));
