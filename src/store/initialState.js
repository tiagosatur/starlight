export default {
  auth: {
    user: {
      email: "",
      avatar: "",
      error: ""
    },
    signUp: {
      email: "",
      avatar: "",
      error: ""
    },
    token: "",
    isAuthenticated: false
  },
  people: {
    people: [],
    arePeopleLoading: false,
    areAvatarsLoading: false,
    error: "",
    avatarError: false
  },
  person: {
    person: {},
    isPersonLoading: false
  },
  planets: {
    planets: [],
    error: ""
  },
  search: {
    suggestions: [],
    count: null,
    isLoading: false
  },
  species: {
    species: [],
    error: ""
  }
};
