import * as TYPES from '../actionTypes';
import initialState from '../initialState';

export default function authReducer(state = initialState.auth, action) {
    switch(action.type) {
        case TYPES.SIGNUP_PENDING:
        case TYPES.SIGNIN_PENDING:
        case TYPES.LOGOUT_PENDING:
            return state;

        case TYPES.SIGNUP_SUCCESS:
            return {
                ...state,
                signUp: {
                    email: action.payload.email,
                    avatar: '',
                    error: '',
                },
            }
        case TYPES.SIGNUP_FAIL:
            return {
                ...state,
                signUp: {
                    error: action.payload.message,
                },
            }

        case TYPES.SIGNIN_SUCCESS:
            return {
                ...state,
                user: {
                    email: action.payload.email,
                    avatar: '',
                    error: '',
                },
                token: action.payload.token,
                isAuthenticated: true,
            }
        case TYPES.SIGNIN_FAIL:
            return {
                ...state,
                user: {
                    email: '',
                    avatar: '',
                    error: action.payload.message,
                },
            }
        case TYPES.LOGOUT_SUCCESS:
            return {
                ...state,
                user: {
                    email: '',
                    avatar: '',
                    error: '',
                },
                token: '',
                isAuthenticated: false,
            }

        
        default:
            return state;
    }
}