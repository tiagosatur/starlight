import * as TYPES from "../actionTypes";
import initialState from "../initialState";

export default function personReducer(state = initialState.person, action) {
  switch (action.type) {
    case TYPES.GET_PERSON_PENDING:
      return {
        ...state,
        isPersonLoading: true
      };

    case TYPES.GET_PERSON_SUCCESS:
      return {
        ...state,
        person: { ...action.payload, avatar: "" },
        isPersonLoading: false
      };
    case TYPES.GET_PERSON_FAIL:
      return {
        ...state,
        error: action.payload,
        isPersonLoading: false
      };
    case TYPES.SET_PERSON_AVATAR:
      return {
        person: {
          ...state.person,
          avatar: action.payload
        }
      };
    default:
      return state;
  }
}
