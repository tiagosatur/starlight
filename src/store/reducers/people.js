import * as TYPES from "../actionTypes";
import initialState from "../initialState";
import { setPeopleAvatars, setPeopleIds } from "../../utils";

export default function peopleReducer(state = initialState.people, action) {
  switch (action.type) {
    case TYPES.GET_PEOPLE_PENDING:
      return { ...state, arePeopleLoading: true };

    case TYPES.GET_PEOPLE_SUCCESS:
      return {
        ...state,
        people: setPeopleIds(action.payload),
        arePeopleLoading: false
      };

    case TYPES.GET_PEOPLE_FAIL:
      return {
        people: action.payload,
        arePeopleLoading: false
      };

    case TYPES.SET_PEOPLE_AVATAR_PENDING:
      return {
        ...state,
        areAvatarsLoading: true
      };

    case TYPES.SET_PEOPLE_AVATAR_SUCCESS:
      return {
        people: setPeopleAvatars(state.people, action.payload),
        areAvatarsLoading: false
      };

    case TYPES.SET_PEOPLE_AVATAR_FAIL:
      return {
        ...state,
        avatarError: true
      };

    default:
      return state;
  }
}
