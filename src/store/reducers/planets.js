import * as TYPES from '../actionTypes';
import initialState from '../initialState';


export default function planetsReducer(state = initialState.planets, action) {
    switch(action.type) {
        case TYPES.GET_PLANETS_PENDING:
            return state;

        case TYPES.GET_PLANETS_SUCCESS:
            return {
                planets: action.payload,
            }

        case TYPES.GET_PLANETS_FAIL:
            
            return {
                planets: action.payload,
            }
        default:
            return state;
    }
}