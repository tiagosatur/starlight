import * as TYPES from '../actionTypes';
import initialState from '../initialState';


export default function speciesReducer(state = initialState.species, action) {
    switch(action.type) {
        case TYPES.GET_SPECIES_PENDING:
            return state;

        case TYPES.GET_SPECIES_SUCCESS:
            return {
                species: action.payload,
            }
            
        case TYPES.GET_SPECIES_FAIL:
            
            return {
                species: action.payload,
            }
        default:
            return state;
    }
}