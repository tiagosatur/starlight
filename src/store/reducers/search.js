import * as TYPES from "../actionTypes";
import initialState from "../initialState";
import { setPeopleIds } from "../../utils";

export default function searchReducer(state = initialState.search, action) {
  switch (action.type) {
    case TYPES.POST_SEARCH_PENDING:
      return {
        ...state,
        isLoading: true
      };

    case TYPES.POST_SEARCH_SUCCESS:
      const convertObjToArray = Object.values(action.payload.results);
      return {
        suggestions: setPeopleIds(convertObjToArray),
        count: action.payload.count,
        isLoading: false
      };
    case TYPES.POST_SEARCH_FAIL:
      return {
        suggestions: [],
        isLoading: false
      };
    case TYPES.RESET_SEARCH_SUGGESTIONS:
      return {
        ...initialState.search
      };
    default:
      return state;
  }
}
