export { default as peopleReducer } from './people';
export { default as personReducer } from './person';
export { default as planetsReducer } from './planets';
export { default as speciesReducer } from './species';
export { default as authReducer } from './auth';
export { default as searchReducer } from './search';