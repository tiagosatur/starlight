const colors = {
  primary: "#928e8e",
  green: "#1dce23",
  secondary: "#f3f568",
  white: "#fff",
  black: "#000000",
  dark: "#333",
  dark02: "#141414",
  primaryGradient: "linear-gradient(135deg, #667eea 0%, #764ba2 100%)",
  transparent: "transparent",
  logo: "#fff"
};

export default colors;
