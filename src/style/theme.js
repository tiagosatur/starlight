import { css } from "styled-components";
import { border, boxShadow } from ".";
export const blackNeomorphismShadow = css`
  background: linear-gradient(145deg, #121212, #151515);
  border-radius: ${border.radius.default};
  box-shadow: ${boxShadow.default};
`;
