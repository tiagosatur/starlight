export default {
  default: "10px 10px 20px #080808, -10px -10px 20px #202020",
  active: "10px 10px 20px #000000, -10px -10px 20px #2f2f2f"
};
