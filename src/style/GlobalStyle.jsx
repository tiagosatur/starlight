import { createGlobalStyle } from "styled-components";
import colors from "./colors";
import boxShadow from "./boxShadow";
import border from "./border";

const GlobalStyle = createGlobalStyle`
  *, 
  *:before, 
  *:after {
      box-sizing: border-box;
  }

  html,
  body,
  #root {
    display: flex;
    flex: 1;
    flex-direction: column;
    flex-wrap: wrap;
    min-height: 100%;
  }

  html {
    font-size: 62.5%;  /* 1rem = 10px */
  }

  body {
    background-color: ${({ whiteColor }) =>
      whiteColor ? colors.white : colors.dark02};
    background-size: cover;
    color: ${colors.primary};
    font-size: 1.6rem;
    font-family: Lekton;
    line-height: 1.5;
  }

  a {
     text-decoration: none;
     border-radius: ${border.radius.default};
     &:hover {
      box-shadow: ${boxShadow.active};
     }
  }

  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  ::selection {
    background-color: ${colors.black};
    color: ${colors.primary};
  }

  input {
    &:-internal-autofill-selected,
    &:-webkit-autofill {
      background-color: rgba(0, 0, 0, .7) !important;
    }
  }

  /* Scrollbar */
  ::-webkit-scrollbar {
    width: 10px;
  }
  
  /* Track */
  ::-webkit-scrollbar-track {
    background: ${colors.black};; 
  }
   
  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${colors.primary};
    border-radius: 0.6rem;
  }
  
  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555; 
  }

  textarea,
  button,
  select,
  input {
    transition: box-shadow .4s;
    &:focus {
      outline: none;
      box-shadow: ${boxShadow.active};
    }
  }



`;

export default GlobalStyle;
