const breakpoints = {
  xs: "0",
  sm: "425px",
  md: "768px",
  lg: "1024px",
  xd: "1440px",
  wd: "2000px"
};

export const device = {
  mobile: `(min-width: ${breakpoints.sm})`,
  tablet: `(min-width: ${breakpoints.md})`,
  laptop: `(min-width: ${breakpoints.lg})`,
  desktop: `(min-width: ${breakpoints.xd})`,
  widescreen: `(min-width: ${breakpoints.wd})`
};
