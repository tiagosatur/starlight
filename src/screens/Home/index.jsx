import React, { useState, useEffect } from "react";
import _ from "lodash";
import { Row, Col } from "react-flexbox-grid";

import { useScreenWidth } from "../../utils";

import {
  Flex,
  PeopleList,
  Select,
  Title,
  Text,
  PeopleListPlaceholder
} from "../../components";
import { useAction, useDatabase, useStore, getPersonId } from "../../utils";

const Home = routerProps => {
  const { actions } = useAction();
  const {
    peopleReducer: { people, arePeopleLoading, areAvatarsLoading }
  } = useStore();

  useEffect(() => {
    getPeopleThenAvatars();
    // getPlanetsAction();
    // getSpeciesAction();
  }, []);

  const [eyeColorList, setEyeColorList] = useState([]);
  const [selectedEyeColor, setSelectedEyeColor] = useState("");
  const { getById, getAll } = useDatabase();

  useEffect(() => {
    setEyeColorList(people);
  }, [people]);

  useEffect(() => {
    const filterEyeColor = people.filter(
      item => item.eye_color == selectedEyeColor
    );
    setEyeColorList(selectedEyeColor === "all" ? people : filterEyeColor);
  }, [selectedEyeColor]);

  const handleChangePeopleList = e => setSelectedEyeColor(e.target.value);

  const getPeopleThenAvatars = async () => {
    await actions.getPeople().then(async () => {
      await getAllIndexedDbAvatars().then(res => {
        actions.setPeopleAvatar(res);
      });
    });
  };

  const getAllIndexedDbAvatars = () =>
    getAll().then(peopleFromDb => peopleFromDb);

  const screenWidth = useScreenWidth();

  return (
    <>
      <Row>
        <Col xs>
          <Flex justify="flex-end">
            <Select
              name="eyeColorListFilter"
              placeholder="Eye color filter"
              handleChange={handleChangePeopleList}
              value={selectedEyeColor}
              list={people && _.uniqBy(people, "eye_color")}
              optionValue="eye_color"
              optionLabel="eye_color"
              uniqueKey="url"
            />
          </Flex>
        </Col>
      </Row>

      <Row>
        <Col xs>
          {arePeopleLoading ? (
            <PeopleListPlaceholder
              column={screenWidth >= 1080 ? 4 : screenWidth >= 768 ? 3 : 2}
              width={
                screenWidth >= 1024 ? 1180 : screenWidth >= 768 ? 700 : 340
              }
            />
          ) : (
            <PeopleList
              data={{
                list: eyeColorList,
                routerProps,
                areAvatarsLoading
              }}
              actions={{}}
            />
          )}
        </Col>
      </Row>

      {/* {
          planets && planets.map(planet => (
            <Text white key={planet.url}>{ planet.name }</Text>
          ))
        }
        {
          species && species.map(specie => (
            <Text key={specie.url}>{ specie.name }</Text>
          ))
        } */}
    </>
  );
};

export default Home;
