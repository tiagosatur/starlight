import React from 'react';
import styled from 'styled-components';
import { GiInterceptorShip, GiSpaceship } from 'react-icons/gi'
//Glow effect
//https://codepen.io/GeorgePark/pen/MrjbEr
// https://codepen.io/FelixRilling/pen/qzfoc

export default function () {
    return (
      <>
        <Number404>
          4
          <GiInterceptorShip size='20rem' color="#fff" />
          4
        </Number404>
        <Sorry>
          Sorry, but this page was stolen.
          Try the search!
        </Sorry>
      </>
    )
}

const Number404 = styled.p`
  align-items: center;
  display: flex;
  justify-content: center;
  font-size: 25rem;
  margin: 0;
`;

const Sorry = styled.p`
  align-items: center;
  display: flex;
  justify-content: center;
  font-size: 2rem;
`;