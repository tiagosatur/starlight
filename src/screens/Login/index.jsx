import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import { useSelector } from "react-redux";
import styled, { css } from "styled-components";
import { useForm } from "react-hook-form";
import { Grid, Row, Col } from "react-flexbox-grid";

import { Button, Input, Text, RouterLink } from "../../components";
import { useAction } from "../../utils/hooks";
import { colors, blackNeomorphismShadow, device } from "../../style";

const Login = ({ match: { path, url }, history }) => {
  const { actions } = useAction();

  const {
    authReducer: { isAuthenticated }
  } = useSelector(state => state);

  useEffect(() => {
    if (isAuthenticated) history.push("/");
  }, [isAuthenticated]);

  const { register, handleSubmit, errors } = useForm();

  const handleSignIn = values => {
    console.log("handleSignIn values", values);
    actions.signIn(values);
  };

  const handleSignUp = values => {
    actions.signUp(values);
  };

  return (
    <Router>
      <Grid>
        <Row>
          <Col xs="12">
            <Nav>
              <Tabs>
                <li>
                  <RouterLink to={`${path}/signin`} label="Sign in" />
                </li>
                <li>
                  <RouterLink to={`${path}/signup`} label="Sign up" />
                </li>
              </Tabs>
            </Nav>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <Switch>
              <Route
                path={`${path}/signin`}
                component={() => (
                  <SignInComponent
                    data={{
                      errors
                    }}
                    actions={{
                      register,
                      handleSubmit,
                      handleSignIn
                    }}
                  />
                )}
              />
              <Route
                path={`${path}/signup`}
                component={() => (
                  <SignUpComponent
                    data={{ errors }}
                    actions={{
                      handleSignUp,
                      register,
                      handleSubmit
                    }}
                  />
                )}
              />
              <Redirect to="/auth/signin" />
            </Switch>
          </Col>
        </Row>
      </Grid>
    </Router>
  );
};

const SignInComponent = ({
  data: { errors },
  actions: { handleSignIn, register, handleSubmit }
}) => {
  return (
    <AuthForm onSubmit={handleSubmit(handleSignIn)}>
      <Text mb={3} center>
        Sign in to have an awesome experience!
      </Text>
      <Input
        name="email"
        placeholder="Email"
        register={register({ required: true })}
        errors={errors}
      />
      <Input
        type="password"
        name="password"
        register={register({ required: true })}
        placeholder="Password"
        errors={errors}
      />
      <Button
        type="submit"
        id="login-form-btn"
        solid
        style={{ marginTop: "1rem" }}
      >
        Let's go!
      </Button>
    </AuthForm>
  );
};

const SignUpComponent = ({
  data: { errors },
  actions: { register, handleSubmit, handleSignUp }
}) => {
  return (
    <AuthForm onSubmit={handleSubmit(handleSignUp)}>
      <Text mb={3} center>
        Create a powerful account
      </Text>
      <Input
        name="email"
        id="login-form-email-input"
        placeholder="Email"
        register={register({ required: true })}
        errors={errors}
      />
      <Input
        type="password"
        name="password"
        register={register({ required: true })}
        placeholder="Password"
        errors={errors}
      />
      <Button
        type="submit"
        id="login-form-btn"
        solid
        style={{ marginTop: "1rem" }}
      >
        Let's do it!
      </Button>
    </AuthForm>
  );
};

const AuthForm = styled.form`
  ${blackNeomorphismShadow};
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  padding: 8rem 4rem;

  @media ${device.tablet} {
    width: 50%;
  }

  @media ${device.laptop} {
    width: 40%;
  }

  @media ${device.desktop} {
    width: 30%;
  }

  @media ${device.widescreen} {
    width: 20%;
  }
`;

const Tabs = styled.ul`
  align-items: center;
  display: flex;

  & > li:not(:last-child) {
    margin-right: 2.4rem;
  }

  a {
    padding: 1rem 2.8rem;
    display: inline-block;
  }
`;

const Nav = styled.nav`
  display: flex;
  justify-content: center;
  margin: 3.2rem;
`;

export default Login;
