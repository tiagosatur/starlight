import React, { useState, useEffect, useCallback } from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { FaUpload, FaEdit } from "react-icons/fa";
import { Row, Col } from "react-flexbox-grid";
import Cropper from "react-easy-crop";

import { getCroppedImg } from "../../utils";
import { blackNeomorphismShadow, device, colors } from "../../style";
import { useAction, useDatabase } from "../../utils/hooks";
import { PersonInfo, Button, ThreeBounceLoader } from "../../components";

const Person = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(2);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const [croppedImage, setCroppedImage] = useState(null);
  const [isAvatarLoading, setAvatarLoading] = useState(false);

  const { personId } = useParams();
  const { actions } = useAction();
  const { add, getByID, update } = useDatabase();

  useEffect(() => {
    getPersonThenAvatar();
  }, [personId]);

  const {
    personReducer: {
      person,
      person: { avatar },
      isPersonLoading
    }
  } = useSelector(state => state);

  useEffect(() => {
    avatar !== "" && setCroppedImage(avatar);
  }, [avatar]);

  const handleSelectFile = e => {
    const target = e.target.files[0];
    const img = URL.createObjectURL(target);

    setSelectedFile(img);
  };

  const onCropChange = crop => setCrop(crop);

  const onZoomChange = zoom => setZoom(zoom);

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels);
  }, []);

  const saveCroppedImage = useCallback(async () => {
    setAvatarLoading(true);
    try {
      const croppedImage = await getCroppedImg(selectedFile, croppedAreaPixels);
      setSelectedFile(null);

      const findAvatar = await getAvatarById(personId);

      const wasAvatarFound = findAvatar !== undefined;
      wasAvatarFound
        ? await update({
            personId: parseInt(personId),
            name: person.name,
            avatar: croppedImage
          })
        : await add({
            personId: parseInt(personId),
            name: person.name,
            avatar: croppedImage
          });

      const findAddedAvatar = await getAvatarById(personId);

      setCroppedImage(findAddedAvatar.avatar);
      setAvatarLoading(false);
    } catch (e) {
      console.error(e);
    }
  }, [croppedAreaPixels]);

  const discardImage = () => {
    setSelectedFile(null);
  };

  const getAvatarById = async personId => await getByID(personId);

  const getPersonThenAvatar = async () => {
    const id = personId;
    setAvatarLoading(true);
    actions.setPersonAvatar(null);
    await actions.getPerson(personId).then(async () => {
      await getAvatarById(personId).then(res => {
        res !== undefined && actions.setPersonAvatar(res.avatar);
        setAvatarLoading(false);
      });
    });
  };

  return (
    <Container>
      <Row>
        <Col xs={12} lg={6}>
          <ImageContainer>
            {!!selectedFile ? (
              <>
                <Cropper
                  image={selectedFile}
                  crop={crop}
                  zoom={zoom}
                  aspect={3 / 3}
                  onCropChange={onCropChange}
                  onCropComplete={onCropComplete}
                  onZoomChange={onZoomChange}
                />
              </>
            ) : (
              <>
                {croppedImage !== "" &&
                croppedImage !== null &&
                croppedImage !== undefined ? (
                  <CroppedImage source={croppedImage}>
                    <EditImageContainer className="editImageContainer">
                      <FaEdit size="2rem" className="icon" />
                      <ChangeText white>Change image</ChangeText>
                      <FileInputEdit type="file" onChange={handleSelectFile} />
                    </EditImageContainer>
                  </CroppedImage>
                ) : isAvatarLoading ? (
                  <ThreeBounceLoader color={colors.green} />
                ) : (
                  <>
                    <FaUpload size="5rem" className="icon" />
                    <FileInput type="file" onChange={handleSelectFile} />
                  </>
                )}
              </>
            )}
          </ImageContainer>

          {!!selectedFile && (
            <ButtonGroup>
              <Button onClick={saveCroppedImage} primary solid>
                Save image
              </Button>
              <Button onClick={discardImage} outlined primary>
                Discard
              </Button>
            </ButtonGroup>
          )}
        </Col>

        <Col xs={12} lg={6}>
          <PersonInfo data={{ person, isPersonLoading }} />
        </Col>
      </Row>
    </Container>
  );
};

const Container = styled.div`
  padding-bottom: 15rem;
`;

const ImageContainer = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  justify-content: center;
  min-height: 25rem;
  overflow: hidden;
  padding: 3rem;
  position: relative;
  width: auto;

  &:hover {
    .icon {
      fill: white;
    }

    .editImageContainer {
      transform: translateY(0);
    }
  }

  .icon {
    transition: all 0.8s;
  }
`;

const FileInput = styled.input`
  border: 1px solid red;
  height: 100%;
  position: absolute;
  opacity: 0;
  width: 100%;
`;

const EditImageContainer = styled.div`
  background-color: rgba(0, 0, 0, 0.7);
  bottom: 0;
  height: 5rem;
  position: absolute;
  right: 0;
  width: 100%;

  transition: all 0.4s;

  @media ${device.laptop} {
    transform: translateY(5rem);
  }

  .icon {
    pointer-events: none;
    position: absolute;
    left: 35%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
`;

const FileInputEdit = styled.input`
  cursor: pointer;
  height: 100%;
  opacity: 0;
  width: 100%;
`;

const ChangeText = styled.span`
  position: absolute;
  top: 30%;
  left: 42%;
`;

const CroppedImage = styled.div`
  ${blackNeomorphismShadow};
  background: url(${({ source }) => source && source});
  background-repeat: no-repeat;
  background-size: cover;
  height: 30rem;
  width: 100%;
`;

const ButtonGroup = styled.div`
  margin-top: 2rem;
  display: flex;
  justify-content: center;

  > button:not(:last-of-type) {
    margin-right: 1.6rem;
  }
`;

export default Person;
