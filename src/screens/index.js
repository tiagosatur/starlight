export { default as Login } from './Login';
export { default as Home } from './Home';
export { default as NotFound } from './NotFound';
export { default as Person } from './Person';