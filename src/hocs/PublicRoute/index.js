import React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from '../../hocs';

const PublicRoute = ({component: Component, ...rest}) => {
    return (

        <Route {...rest} render={props => (
                <Layout 
                  data={{
                      routerProps: {...props}
                  }}
                >
                    <Component {...props} />
                </Layout>

        )} />
    );
};

export default PublicRoute;