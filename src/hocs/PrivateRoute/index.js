import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Layout } from '../../hocs';

const PrivateRoute = ({component: Component, ...rest}) => {
    const { 
        authReducer: {
          isAuthenticated,
        },
    } = useSelector(state => state);

    return (

        <Route {...rest} render={props => {
            
            return !isAuthenticated ?
                <Layout 
                    data={{
                        routerProps: {...props}
                    }}
                >
                    <Component {...props} />
                </Layout>
            : <Redirect to="/auth" />
        }} />
    );
};

export default PrivateRoute;