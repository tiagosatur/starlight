import React from "react";
import { Grid } from "react-flexbox-grid";

import { Header } from "../../components";

const Layout = ({ data: { routerProps }, children }) => {
  return (
    <Grid>
      <Header
        data={{
          routerProps
        }}
      />
      {children}
    </Grid>
  );
};

export default Layout;
