export default function() {
    return 'fake-token-' + Math.random().toString(36).substr(2, 9);
}