import { useDispatch } from "react-redux";

import {
  getPeople,
  getPlanets,
  getSpecies,
  signIn,
  signUp,
  logout,
  getPerson,
  search,
  resetSuggestions,
  setPeopleAvatar,
  setPersonAvatar
} from "../../store/actions";

const useAction = () => {
  const dispatch = useDispatch();

  return {
    actions: {
      getPeople: data => dispatch(getPeople(data)),
      getPlanets: data => dispatch(getPlanets(data)),
      getSpecies: data => dispatch(getSpecies(data)),
      signIn: data => dispatch(signIn(data)),
      signUp: data => dispatch(signUp(data)),
      logout: data => dispatch(logout(data)),
      getPerson: data => dispatch(getPerson(data)),
      search: data => dispatch(search(data)),
      resetSuggestions: () => dispatch(resetSuggestions()),
      setPeopleAvatar: data => dispatch(setPeopleAvatar(data)),
      setPersonAvatar: data => dispatch(setPersonAvatar(data))
    }
  };
};

export default useAction;
