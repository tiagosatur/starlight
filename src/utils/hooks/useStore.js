import { useSelector } from "react-redux";

export default function useStore() {
  const {
    authReducer,
    peopleReducer,
    planetsReducer,
    speciesReducer,
    personReducer,
    searchReducer
  } = useSelector(state => state);
  return {
    authReducer,
    peopleReducer,
    planetsReducer,
    speciesReducer,
    personReducer,
    searchReducer
  };
}
