import { useState, useEffect } from "react";

const getWidth = () =>
  window.screen.width ||
  window.visualViewport ||
  window.innerWidth ||
  document.documentElement.clientWidth ||
  document.body.clientWidth;

export default function useScreenWidth(currentWidth) {
  let [width, setWidth] = useState(getWidth());

  useEffect(() => {
    let timeoutId = null;
    const resizeListener = () => {
      clearTimeout(timeoutId);

      timeoutId = setTimeout(() => {
        return setWidth(getWidth());
      }, 150);
    };

    window.addEventListener("resize", resizeListener);

    return () => {
      window.removeEventListener("resize", resizeListener);
    };
  }, [currentWidth]);

  return width;
}
