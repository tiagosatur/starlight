import { useIndexedDB } from "react-indexed-db";

export default function useDatabase() {
  const {
    add,
    update,
    deleteRecord,
    clear,
    getByID,
    getByIndex,
    getAll,
    openCursor
  } = useIndexedDB("people");

  return {
    add,
    update,
    deleteRecord,
    clear,
    getByID,
    getByIndex,
    getAll,
    openCursor
  };
}
