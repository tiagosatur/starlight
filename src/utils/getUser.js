export default async function getUser() {
     const user = await JSON.parse(window.localStorage.getItem('user'));

     return new Promise((resolve, reject) => {
          setTimeout(() => {
              if (user !== null) {
                  resolve({ success: true, ...user });
              } else {
                  reject({message: 'User not registered'});
              }
          }, 1500);
      });
}