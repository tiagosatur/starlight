const setPeopleAvatars = (peopleList, avatarList) => {
  return peopleList.map(person => {
    let matchId = avatarList.find(item => item.personId === person.id);

    return matchId === undefined
      ? person
      : { avatar: matchId.avatar, ...person };
  });
};

export default setPeopleAvatars;
