export default async function deleteUser() {
    const user = await window.localStorage.removeItem('user');

    return new Promise((resolve, reject) => {
         setTimeout(() => {
             if (user !== null) {
                 resolve({ success: true, message: 'User deleted from Local Storage' });
             } else {
                 reject({success: true, message: 'User could not be deleted'});
             }
         }, 2000);
     });
}