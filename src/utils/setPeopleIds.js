import { getPersonId } from ".";

const setPeopleIds = peopleList => {
  return peopleList.map(person => ({
    id: parseInt(getPersonId(person.url)),
    ...person
  }));
};

export default setPeopleIds;
