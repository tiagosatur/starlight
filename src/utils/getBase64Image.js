export default function getBase64Image(file, type = "base64") {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = async () => await resolve(reader.result);
    reader.onerror = error => reject(error);
    type === "blob"
      ? (reader.onloadend = () => {
          reader.readAsDataURL(file);
        })
      : reader.readAsDataURL(file);
  });
}
