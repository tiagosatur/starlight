import axios from 'axios';

import { getUser, deleteUser, generateToken } from '../utils';

const BASE_PATH = 'https://swapi.co/api';
const PATHS = {
    PEOPLE_PATH: `${BASE_PATH}/people`,
    PLANETS_PATH: `${BASE_PATH}/planets`,
    SPECIES_PATH: `${BASE_PATH}/species`
}

export default {
    get: {
        peopleService: () => axios.get(PATHS.PEOPLE_PATH).catch(e => e.response),
        personService: personId => axios.get(`${PATHS.PEOPLE_PATH}/${personId}`).catch(e => e.response),
        planetsService: () => axios.get(PATHS.PLANETS_PATH).catch(e => e.response),
        speciesService: () => axios.get(PATHS.SPECIES_PATH).catch(e => e.response),
        searchPeopleService: term => axios.get(`${PATHS.PEOPLE_PATH}/?search=${term}`).catch(e => e.response),
    },
    post:  {
        signUpService: async values => {
            const { email, password } = values;
            const user = {
              email: email,
              password: password,
              token: generateToken(),
            }
            if (typeof(Storage) !== "undefined") 
              window.localStorage.setItem('user', JSON.stringify(user))
            
            const retrieve = await getUser();
            return retrieve
        },
        signInService: async values => {
            const user = await getUser();
            return user.email === values.email && user.password === values.password 
            ? {success: true, ...user}
            : { success: false, message: 'User not registered' }
        },
        logoutService: async () => {
            const user = await deleteUser();
            return user;
        },
    }
}
