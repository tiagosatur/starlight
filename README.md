## Goal

This application shows a character list on the main page and let the user search by name. Additionally the user can filter them by eye color and upload a character avatar.

![](https://s5.gifyu.com/images/Starlight-Google-Chrome-2020-03.gif)

## Install

`yarn install`

## Run

`yarn start`

Go to [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Main features

- Consume the [Swappi Api](https://swapi.co/api) to display characters, if there is a local image, this image is displayed in the list item
- Uses routes
- Has a search engine with debounced search to avoid overwhelm the api with requests
- In the character page, the user is able see character details and upload an image that is converted to base64 and stored in the indexedDB api

## Main dependencies

- React
- Redux
- React Router
- Styled Components
- React indexed db
- React easy crop
- React icons
- Axios
- Lodash
- Flexbox grid

## Contributions

Wanna suggest an improvement? Fell free to comment or make a pull request
